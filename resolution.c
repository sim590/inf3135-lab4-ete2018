#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define PRG "resolution"

struct Couple {
    double elem1;
    double elem2;
};

struct Solution {
    unsigned int nbSolutions;    // Le nombre de solutions de l'équation
    union {
        double solution1;        // La solution unique quand delta = 0
        struct Couple solution2; // Les deux solutions quand delta > 0
    } solutions;
};

struct Solution resoudre_equation(double a, double b, double c) {
    double d = pow(b, 2) - 4*a*c;

    if (d > 0)
        return (struct Solution) {
            .nbSolutions = 2,
            .solutions.solution2 = (struct Couple) { (-b - sqrt(d))/2/a, (-b + sqrt(d))/2/a }
        };
    else if (d == 0) {
        return (struct Solution) {
            .nbSolutions = 1,
            .solutions.solution1 = -b / 2 / a
        };
    } else {
        return (struct Solution) { 0 };
    }
}

void afficher_solution(const struct Solution* sol) {
    switch (sol->nbSolutions) {
        case 0:
            printf("Aucune solution pour l'équation\n");
            break;
        case 1:
            printf("L'équation admet une solution: %f\n", sol->solutions.solution1);
            break;
        case 2:
            printf("L'équation admet deux solutions: %f et %f\n",
                    sol->solutions.solution2.elem1,
                    sol->solutions.solution2.elem2);
            break;
        default:
            fprintf(stderr, "%s: erreur: struct solution invalide.", PRG);
    }
}

int main(int argc, char *argv[]) {
    if (argc != 4) {
        fprintf(stderr, "%s: erreur: Le nombre d'arguments est invalide.\n", PRG);
        return 1;
    }

    double a = strtof(argv[1], NULL), b = strtof(argv[2], NULL), c = strtof(argv[3], NULL);

    struct Solution s = resoudre_equation(a, b, c);
    afficher_solution(&s);
    return 0;
}

/* vim: set ts=4 sw=4 tw=120 et :*/

