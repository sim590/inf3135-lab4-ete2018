CC     = gcc
CFLAGS = --std=c11 $(if $(DEBUG),-ggdb -O0,-O2) -Wall -Wextra
LFLAGS = -lm
EXECS  = progStruct resolution

all: $(EXECS)

resolution: resolution.o
progStruct: progStruct.o

%: %.o
	$(CC) $< -o $@ $(LFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c $<

.PHONY: clean

clean:
	rm -f *.o $(EXECS)
