#include <math.h>
#include <stdio.h>

struct Point { // Un point en 2D
    double x;  // Sa coordonnée x
    double y;  // Sa coordonnée y
};

struct Segment {
    struct Point p1;
    struct Point p2;
};

struct Triangle {
    struct Point points[3];
};

// Prototypes
void initialiserSegment(struct Segment *segment,
        double x1, double y1,
        double x2, double y2);
double longueurSegment(const struct Segment *segment);
void initialiserTriangle(struct Triangle *triangle,
        double x1, double y1,
        double x2, double y2,
        double x3, double y3);
double perimetreTriangle(const struct Triangle *triangle);

int main(int argc, char* argv[]) {
    if(argc != 1) {
        fprintf(stderr, "USAGE: %s\n", argv[0]);
        return -1;
    }

    // Tests des fonctions
    struct Segment segment1;
    initialiserSegment(&segment1, 1,2, 3,4);
    printf("La longueur du segment est: %f\n", longueurSegment(&segment1));

    struct Triangle t;
    initialiserTriangle(&t, 0,0, 0,1, 1,0);
    printf("Le périmètre du triangle est: %f\n", perimetreTriangle(&t));
}

// Implémentations
void initialiserSegment(struct Segment *segment,
        double x1, double y1,
        double x2, double y2) {
    if(segment == NULL) {
        fprintf(stderr, "Le pointeur de segment est NULL\n");
        return;
    }
    
    /* (*segment).p1.x = x1; */
    segment->p1.x = x1;
    segment->p1.y = y1;
    segment->p2.x = x2;
    segment->p2.y = y2;
}

double longueurSegment(const struct Segment *segment) {
    if(segment == NULL) {
        fprintf(stderr, "Le pointeur de segment est NULL\n");
        return -1;
    }

    double longueur = segment->p2.x - segment->p1.x;
    double hauteur = segment->p2.y - segment->p1.y;
    return sqrt( pow(longueur, 2) + pow(hauteur, 2) );
}

void initialiserTriangle(struct Triangle *triangle,
        double x1, double y1,
        double x2, double y2,
        double x3, double y3) {
    if(triangle == NULL) {
        fprintf(stderr, "Le pointeur de triangle est NULL\n");
        return;
    }

    /* triangle->points[0] = (struct Point) {x1, y1}; */
    /* triangle->points[1] = (struct Point) {x2, y2}; */
    /* triangle->points[2] = (struct Point) {x3, y3}; */

    *triangle = (struct Triangle) {.points =
        {(struct Point) {x1,y1},
         (struct Point) {x2,y2},
         (struct Point) {x3,y3}}};
}

double perimetreTriangle(const struct Triangle *triangle) {
    struct Segment cote1 = (struct Segment) {.p1 = triangle->points[0],
                                             .p2 = triangle->points[1]};
    struct Segment cote2 = (struct Segment) {.p1 = triangle->points[1],
                                             .p2 = triangle->points[2]};
    struct Segment cote3 = (struct Segment) {.p1 = triangle->points[2],
                                             .p2 = triangle->points[0]};
    double longueurCote1 = longueurSegment(&cote1);
    double longueurCote2 = longueurSegment(&cote2);
    double longueurCote3 = longueurSegment(&cote3);
    return longueurCote1 + longueurCote2 + longueurCote3;
}
